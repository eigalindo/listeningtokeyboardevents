let box = document.getElementById("box")
let log = document.getElementById("log");

let boxTop = 400
let boxLeft = 400

document.addEventListener('keydown', logKey);


function logKey(e) {
    log.textContent += ` ${e.code}`;
    e = e || window.event

    switch(e.code){
        case 'ArrowDown' : 
            box.style.marginTop = boxTop + 20 + "px";
            boxTop+=20;
        break; 
        case 'ArrowUp': box.style.marginTop = boxTop - 20 + "px";
            boxTop-=20;
        break; 
        case 'ArrowLeft' : box.style.marginLeft = boxLeft -20 + "px";
            boxLeft -=20;
            break;
        case 'ArrowRight' : box.style.marginLeft = boxLeft +20 + "px";
            boxLeft +=20;
            break;
        default: 
            box.style.marginTop = boxTop;
            box.style.marginLeft = boxLeft;  
    }
}
